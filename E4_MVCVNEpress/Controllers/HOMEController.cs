﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E4_MVCVNEpress.Controllers
{
    public class HOMEController : Controller
    {
        // GET: HOME
        public ActionResult Index()
        {
            return View();
        }
        // GET: HOME/INFO
        public ActionResult INFO()
        {
            return View();
        }
    }
}